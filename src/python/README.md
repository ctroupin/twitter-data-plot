
## Virtual environment

```bash
mkvirtualenv -p /usr/local/bin/python3.10 twitter-data
```

```bash
ipython kernel install --name "twitter-data" --user
```
