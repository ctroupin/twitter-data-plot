import os
import glob
import numpy as np
import netCDF4
import datetime
import cmocean
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
plt.rcParams.update({'font.size': 14})
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.mpl.ticker as cartopyticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
coast = cfeature.GSHHSFeature(scale="f")
from matplotlib.font_manager import FontProperties
fa_dir = r"/home/ctroupin/Downloads/fontawesome-free-5.0.13/use-on-desktop/"
fp1 = FontProperties(fname=os.path.join(fa_dir, "Font Awesome 5 Free-Solid-900.otf"))


# Files and directories
datadir = "../../data"
figdir = "../../figures/"

cmemsdatadir = os.path.join(datadir, "CMEMS/")
argodatadir = os.path.join(datadir, "Argo")

# Coordinate systems
domain = [0, 12., 40., 45.]
maincrs = ccrs.Mercator(central_longitude=0.5 * (domain[0] + domain[1]),
                        min_latitude=domain[2], max_latitude=domain[3],
                        latitude_true_scale=0.5 * (domain[2] + domain[3]))
datacrs = ccrs.PlateCarree()


def get_time_temp(datafile):
    """Read the temperature from a netCDF file from CMEMS INSTAC
    """
    with netCDF4.Dataset(datafile) as nc:
        T = nc.get_variables_by_attributes(standard_name="sea_water_temperature")[0][:,1]
        timevar = nc.get_variables_by_attributes(standard_name="time")[0]
        depth = nc.get_variables_by_attributes(standard_name="depth")[0][:]
        timeunit = timevar.units
        dates = netCDF4.num2date(timevar[:], timeunit, only_use_cftime_datetimes=False)
        wmocode = nc.wmo_platform_code
    return dates, T, wmocode, depth

def get_mean_position(datafile):
    """Extract the mean position and the WMO code from a netCDF file
    from CMEMS INSTAC
    """
    with netCDF4.Dataset(datafile) as nc:
        lonmean = nc.get_variables_by_attributes(standard_name="longitude")[0][:].mean()
        latmean = nc.get_variables_by_attributes(standard_name="latitude")[0][:].mean()
        wmocode = nc.wmo_platform_code
        return lonmean, latmean, wmocode

def read_wind_dates(datafile):
    """Read the wind intensity and the dates from a netCDF file"""

    with netCDF4.Dataset(datafile) as nc:
        timevar = nc.get_variables_by_attributes(standard_name="time")[0]
        lon = nc.get_variables_by_attributes(standard_name="longitude")[0][:]
        lat = nc.get_variables_by_attributes(standard_name="latitude")[0][:]
        depth = nc.get_variables_by_attributes(standard_name="depth")[0][:]
        dates = netCDF4.num2date(timevar[:], timevar.units, only_use_cftime_datetimes=False)
        gooddates = np.where((dates <= dateend) & (dates >= datestart))[0]
        wind = nc.get_variables_by_attributes(standard_name="wind_speed")[0][:]

        return dates[gooddates], wind[gooddates]

def load_argo_position(argofile):
    """Load position from Argo float"""
    with netCDF4.Dataset(argofile) as nc:
        lonargo = nc.get_variables_by_attributes(standard_name="longitude")[0][:]
        latargo = nc.get_variables_by_attributes(standard_name="latitude")[0][:]
    return lonargo, latargo

def decorate_map(ax):
    ax.set_extent(domain)

    gl = ax.gridlines(crs=datacrs, draw_labels=True,
                      linewidth=.5, color='k', alpha=0.95, linestyle='--', zorder=2)

    gl.top_labels = False
    gl.right_labels = False
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER

    ax.add_feature(coast, color=".85", zorder=5)
    ax.add_feature(coast, linewidth=.25, zorder=6)
